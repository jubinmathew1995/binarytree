#include <BinaryTree/BinaryTree.h>
#include <vector>
#include <queue>

BinaryTree::BinaryTree() {
    this->root = nullptr;
}

std::vector<int> BinaryTree::levelOrderTraversal() {
    std::vector<int> result;

    std::queue<TreeNode*> q;
    q.push(this->root);

    while(!q.empty()) {
        TreeNode *temp = q.front();
        q.pop();

        result.push_back(temp->getData());

        if(temp->getLeftPtr()) q.push(temp->getLeftPtr());
        if(temp->getRightPtr()) q.push(temp->getRightPtr());
    }

    return(result);
}

// Recursive implementation for in order traversal
void BinaryTree::inOrder(TreeNode *curNode, std::vector<int> &result) {
    if(!curNode) return;

    inOrder(curNode->getLeftPtr(), result);
    result.push_back(curNode->getData());
    inOrder(curNode->getRightPtr(), result);
}

std::vector<int> BinaryTree::inOrderTraversal() {
    std::vector<int> result;

    inOrder(this->root, result);

    return(result);
}

// Recursive implementation for pre order traversal
void BinaryTree::preOrder(TreeNode *curNode, std::vector<int> &result) {
    if(!curNode) return;

    result.push_back(curNode->getData());
    preOrder(curNode->getLeftPtr(), result);
    preOrder(curNode->getRightPtr(), result);
}

std::vector<int> BinaryTree::preOrderTraversal() {
    std::vector<int> result;

    this->preOrder(this->root, result);

    return(result);
}

// Recursive implementation for post order traversal
void BinaryTree::postOrder(TreeNode *curNode, std::vector<int> &result) {
    if(!curNode) return;

    postOrder(curNode->getLeftPtr(), result);
    postOrder(curNode->getRightPtr(), result);
    result.push_back(curNode->getData());
}

std::vector<int> BinaryTree::postOrderTraversal() {
    std::vector<int> result;

    postOrder(this->root, result);

    return(result);
}

void BinaryTree::genRandomBinTreeSample() {
/**
 * @brief Generate the Binary Tree.
 *          10
 *        /    \
 *      20      30
 *     /  \    /  \
 *    40  50  60  70
 */
    this->root = new TreeNode(10);
    this->root->setLeftPtr(new TreeNode(20));
    this->root->setRightPtr(new TreeNode(30));

    this->root->getLeftPtr()->setLeftPtr(new TreeNode(40));
    this->root->getLeftPtr()->setRightPtr(new TreeNode(50));

    this->root->getRightPtr()->setLeftPtr(new TreeNode(60));
    this->root->getRightPtr()->setRightPtr(new TreeNode(70));
}

BinaryTree::~BinaryTree() {
    // Dectructor for the Binary Tree
    if(!this->root) return;

    std::queue<TreeNode*> q;
    q.push(this->root);

    while(!q.empty()) {
        TreeNode *temp = q.front();
        q.pop();
        
        if(temp->getLeftPtr()) q.push(temp->getLeftPtr());
        if(temp->getRightPtr()) q.push(temp->getRightPtr());

        temp->setLeftPtr(nullptr);
        temp->setRightPtr(nullptr);
        delete temp;
    }

    this->root = nullptr;
}