#include <BinaryTree/TreeNode.h>

TreeNode::TreeNode(int val) {
    this->data = val;
    this->left = this->right = nullptr;
}

TreeNode::TreeNode(int val, TreeNode *lt, TreeNode *rt) {
    this->data = val;
    this->left = lt;
    this->right = rt;
}

TreeNode* TreeNode::getLeftPtr() {
    return(this->left);
}

TreeNode* TreeNode::getRightPtr() {
    return(this->right);
}

int TreeNode::getData() {
    return(this->data);
}

void TreeNode::setData(int val) {
    this->data = val;
}

void TreeNode::setLeftPtr(TreeNode *lt) {
    this->left = lt;
}

void TreeNode::setRightPtr(TreeNode *rt) {
    this->right = rt;
}

TreeNode::~TreeNode() {
    this->left = this->right = nullptr;
}
