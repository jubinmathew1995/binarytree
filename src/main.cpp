#include <iostream>
#include <vector>
#include <BinaryTree/BinaryTree.h>

int main() {
    BinaryTree t;
    std::vector<int> out;

    t.genRandomBinTreeSample();

    std::cout<<"Level Order : ";
    out = t.levelOrderTraversal();
    for(auto it: out) {
        std::cout<<it<<" ";
    }
    std::cout<<"\n";
    out.clear();

    std::cout<<"Pre Order : ";
    out = t.preOrderTraversal();
    for(auto it: out) {
        std::cout<<it<<" ";
    }
    std::cout<<"\n";

    std::cout<<"In Order : ";
    out = t.inOrderTraversal();
    for(auto it: out) {
        std::cout<<it<<" ";
    }
    std::cout<<"\n";
    out.clear();

    std::cout<<"Post Order : ";
    out = t.postOrderTraversal();
    for(auto it: out) {
        std::cout<<it<<" ";
    }
    std::cout<<"\n";
    out.clear();
    return 0;
}