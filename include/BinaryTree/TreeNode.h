#ifndef TREE_NODE_H
#define TREE_NODE_H

class TreeNode
{
private:
    int data;
    TreeNode *left;
    TreeNode *right;

public:
    // Constructors for TreeNode class.
    TreeNode(int);
    TreeNode(int, TreeNode *, TreeNode *);

    // Defining the getters for the TreeNode class.
    TreeNode *getLeftPtr();
    TreeNode *getRightPtr();
    int getData();

    // Defining the setters for the Tr4eeNode class.
    void setLeftPtr(TreeNode *);
    void setRightPtr(TreeNode *);
    void setData(int);

    // Destructor for TreeNode class.
    ~TreeNode();
};

#endif