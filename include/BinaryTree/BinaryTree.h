#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <BinaryTree/TreeNode.h>
#include <vector>

class BinaryTree {
private:
    TreeNode *root;
    void inOrder(TreeNode*, std::vector<int>&);
    void preOrder(TreeNode*, std::vector<int>&);
    void postOrder(TreeNode*, std::vector<int>&);

public:
    BinaryTree();

    std::vector<int> levelOrderTraversal();
    std::vector<int> inOrderTraversal();
    std::vector<int> preOrderTraversal();
    std::vector<int> postOrderTraversal();
    void genRandomBinTreeSample();

    ~BinaryTree();

};

#endif